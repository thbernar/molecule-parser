import pytest

from molecule_parser import parse_molecule


class TestMoleculeParser:
    def test_water(self):
        formulae = "H2O"
        assert parse_molecule(formulae) == {"H": 2, "O": 1}

    def test_magnesium_hydroxide(self):
        formulae = "Mg(OH)2"
        assert parse_molecule(formulae) == {"Mg": 1, "O": 2, "H": 2}

    def test_fremy_salt(self):
        formulae = "K4[ON(SO3)2]2"
        assert parse_molecule(formulae) == {"K": 4, "O": 14, "N": 2, "S": 4}

    # Some additionals tests
    def test_salt(self):
        formulae = "NaCl"
        assert parse_molecule(formulae) == {"Na": 1, "Cl": 1}

    def test_hydroxyapatite(self):
        formulae = "Ca10(PO4)6(OH)2"
        assert parse_molecule(formulae) == {"Ca": 10, "P": 6, "O": 26, "H": 2}

    def test_paracetamol(self):
        formulae = "C8H9NO2"
        assert parse_molecule(formulae) == {"C": 8, "H": 9, "N": 1, "O": 2}

    # More tests
    def test_water_complicated(self):
        formulae = "[(H2O)]"
        assert parse_molecule(formulae) == {"H": 2, "O": 1}

    def test_water_super_complicated(self):
        formulae = "[({[H2]}{O})1]"
        assert parse_molecule(formulae) == {"H": 2, "O": 1}
