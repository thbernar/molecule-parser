import re

REGEX_PARSE = r"([A-Z][a-z]?)(\d*)"
REGEX_PARENTHESIS = r"\(([^\(\[\{\)\]\}]+)\)(\d*)"

def parse_molecule(formulae: str) -> dict:
    molecule = MoleculeParser(formulae)
    return molecule.parse()


class MoleculeParser:
    def __init__(self, formulae: str):
        self.formulae = formulae
        self.formulae = re.sub(r"\[|\{", "(", self.formulae)
        self.formulae = re.sub(r"\]|\)", ")", self.formulae)

    def __search_for_nested(self) -> re.Match:
        return re.search(REGEX_PARENTHESIS, self.formulae)

    def __flatten_nested(self, nested_str: str, coef: int):
        flat_mol = ""
        atoms = re.findall(REGEX_PARSE, nested_str)
        for atom, amount in atoms:
            flat_mol = flat_mol + atom + str(int(amount or 1) * int(coef or 1))
        self.formulae = re.sub(REGEX_PARENTHESIS, flat_mol, self.formulae, 1)

    def __parse_flat_formulae(self) -> dict:
        parsed_formulae = {}
        atoms = re.findall(REGEX_PARSE, self.formulae)
        for atom, coef in atoms:
            parsed_formulae[atom] = parsed_formulae.get(atom, 0) + int(coef or 1)
        return parsed_formulae

    def parse(self) -> dict:
        nested = self.__search_for_nested()
        while nested:
            self.__flatten_nested(nested.groups()[0], nested.groups()[1])
            nested = self.__search_for_nested()
        return self.__parse_flat_formulae()
